import mongoengine as mongo


class Service(mongo.Document):
    """
        Represents service (like drobpox, gmail, ect) as data point
    """
    meta = {'collection': 'services', 'allow_inheritance': False}

    name = mongo.StringField()
    active = mongo.BooleanField(default=True)
    url = mongo.StringField()

    def get_data(self):
        """
            allow get any data from service (files, email)
        """
        pass

    def put_data(self):
        """
            allow put any data to service
        """
        pass


class Task(mongo.Document):
    """
        Represents service (like drobpox, gmail, ect) as data point
    """
    meta = {'collection': 'services', 'allow_inheritance': False}

    # django username
    user = mongo.StringField()

