# coding: utf-8
from django.http import HttpResponse, HttpResponseRedirect
import pymongo
from django.shortcuts import render
import sys
from myifttt.settings import db

def index(request):

    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login?next=/')
    try:
        services = db.services.find({"user":request.user.username})
    except:
        print(sys.exc_info())
        services = {}
    return render(request, 'services/index.html', {'services': services, 'user':request.user, 'logged':request.user.is_authenticated()})
