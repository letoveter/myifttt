from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.views import login, logout


urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'services.views.index', name='index'),
    url(r'^services/', include('services.urls')),
    (r'^login/$',  login),
    (r'^logout/$', logout),
    url(r'^admin/', include(admin.site.urls)),
)
